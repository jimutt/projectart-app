import Vuex from 'vuex';

const createStore = () => {
  return new Vuex.Store({
    state: {
      activeSpecies: null,
      activeSpeciesImgSrc: null,
      userEmail: ''
    },
    mutations: {
      setActiveSpecies(state, species) {
        state.activeSpecies = species;
      },
      setActiveSpeciesImgSrc(state, imgSrc) {
        state.activeSpeciesImgSrc = imgSrc;
      },
      setUserEmail(state, userEmail) {
        state.userEmail = userEmail;
      }
    }
  });
};

export default createStore;
